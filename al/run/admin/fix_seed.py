#!/usr/bin/env python
"""
This script takes a seed path as parameter and save the seed into
the original_seed key in the blob bucket.
"""

import os
import sys

seed_path = ""
# noinspection PyBroadException
try:
    seed_path = sys.argv[1]
    os.environ['AL_SEED_STATIC'] = seed_path
except:
    print "fix_seed <seed_path>"
    exit(1)

if __name__ == "__main__":
    import logging

    from assemblyline.al.core.datastore import RiakStore
    from assemblyline.common.importing import module_attribute_by_name
    from assemblyline.al.service import register_service
    from assemblyline.al.common.service_utils import validate_service

    log = logging.getLogger('assemblyline.datastore')
    log.setLevel(logging.WARNING)

    # noinspection PyBroadException
    seed = module_attribute_by_name(seed_path)
    services_to_register = dict(seed['services']['master_list'])

    for service, svc_detail in services_to_register.iteritems():
        try:
            svc_detail = validate_service(svc_detail)

            classpath = svc_detail['classpath']
            config_overrides = svc_detail.get('config', {})
            svc_detail = validate_service(register_service.register(classpath,
                                                                    config_overrides=config_overrides,
                                                                    store_config=False,
                                                                    enabled=svc_detail.get('enabled', True)))

            seed['services']['master_list'][service].update(svc_detail)
        except ValueError as error:
            print "Could not validate service configuration, removed service", service
            print error
            del seed['services']['master_list'][service]

    ds = RiakStore(hosts=seed['datastore']['hosts'])
    print "Seed {seed} loaded and datastore connected to {hosts}".format(seed=seed_path,
                                                                         hosts="|".join(seed['datastore']['hosts']))

    # noinspection PyBroadException
    try:
        target = sys.argv[2]
    except:
        target = raw_input("Where to save to? [seed]: ")
        if not target:
            target = "seed"

    ds.save_blob(target, seed)
    print "Seed '%s' saved to '%s'" % (seed_path, target)
