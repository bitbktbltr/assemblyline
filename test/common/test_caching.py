from assemblyline.test import MockTime
import unittest
from flaky import flaky
from assemblyline.common import caching


class TestCaching(unittest.TestCase):
    def test_timeout(self):
        """Insert a single item and wait for it to expire."""
        with MockTime() as time:
            tc = caching.TimeExpiredCache(5, 0.01)
            tc.add(1, 1)

            # It should be around for four seconds
            for _ in range(4):
                time.advance(1)
                self.assertIsNotNone(tc.get(1))

            # And gone by the time we reach six
            time.advance(2)
            self.assertIsNone(tc.get(1))

    @flaky(max_runs=3, min_passes=1)
    def test_time_order(self):
        """Insert several items and wait for them to expire in order."""
        with MockTime() as time:
            tc = caching.TimeExpiredCache(5, 0.01)

            for ii in range(5):
                tc.add(ii, ii)
                time.advance(1)

            time.advance(0.5)
            tc.add(5, 5)

            for ii in range(5):
                self.assertEqual(tc.get(ii+1), ii+1)
                time.advance(1)
                self.assertIsNone(tc.get(ii))

    def test_SizeExpiredCache(self):
        """Add elements to the size cache checking that it acts like a queue"""
        sc = caching.SizeExpiredCache(5)
        sequence = [10, 5, 2, 3, 1, 80, -3, 10, 'a', None]

        for item in sequence[:5]:
            sc.add(item, item)

        for index in range(5):
            self.assertEqual(sc.get(sequence[index]), sequence[index])
            self.assertIsNone(sc.get(sequence[index + 5]))

            sc.add(sequence[index + 5], sequence[index + 5])

            self.assertIsNone(sc.get(sequence[index]))
