from assemblyline.common import constants


def test_unique_tags():
    """Ensure tag constants are unique."""
    names = set()
    ids = set()
    for n, i in constants.STANDARD_TAG_TYPES:
        assert n not in names
        assert i not in ids
        names.add(n)
        ids.add(i)
