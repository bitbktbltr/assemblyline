from assemblyline.common.iprange import RangeTable


def test_iprange():
    r = RangeTable()
    r['0.0.0.0/24'] = 'blah'

    assert r['0.0.0.0'] == 'blah'
    assert r['0.0.0.1'] == 'blah'
    assert r['0.0.1.1'] is None

    assert r[0] == 'blah'
    assert r[1] == 'blah'
    assert r[1000] is None

    r['0.0.0.1-0.0.0.2'] = {'message': 'not blah'}
    r['0.0.0.200 - 0.0.1.2'] = 'testing'
    r['127.0.0.1/8'] = 'loopback'  # Notice we handle the incorrect CIDR.

    assert r['0.0.0.0'] == 'blah'
    assert r['0.0.0.1'] == {'message': 'not blah'}
    assert r['0.0.1.1'] == 'testing'

    # Make sure the key values occur the right number of times in the full dump
    string = r.dump()
    assert string.count("loopback") == 1
    assert string.count("not blah") == 2
    # Blah was 256, but got trunicated to 200 by testing, then two were replaced by dicts
    assert string.count("'blah'") == 200 - 2
    # testing was 200 to the end of 8 bit block, then 3 in the next block
    assert string.count("'testing'") == 256 - 200 + 3
