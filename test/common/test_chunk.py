from assemblyline.common import chunk


def test_chunks():
    assert chunk.chunked_list([0, 1, 2], 5) == [[0, 1, 2]]
    assert chunk.chunked_list([0, 1, 2], 2) == [[0, 1], [2]]
